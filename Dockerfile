FROM node:14-alpine

WORKDIR ./app

COPY package.json /app
COPY index.md /app
COPY yarn.lock /app 

RUN yarn
RUN yarn build 

CMD yarn start
